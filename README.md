cNoise
======
cNoise is a Python module with a C backend that implements common noise algorithms, including perlin improved, and simplex.

## Obtaining it

    $ git clone git://github.com/AndrewBC/cNoise.git

## Installation

    $ cd cNoise
    $ python setup.py install

## Using it

```python
import cNoise
perlin2d = [[cNoise.perlin2(x, y) for x in range(5)] for y in range(5)]
simplex2d = [[cNoise.simplex2(x, y) for x in range(5)] for y in range(5)]
...
simplex4d = [[[[cNoise.simplex4(x, y, z, w for x in range(5)] for y in range(5)] for z in range(5)] for w in range(5)] 
```    

